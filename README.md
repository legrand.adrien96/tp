## Question 2

le patern d'architecture utilisé est un patern MVC car il nous permet:

-   Une conception claire et efficace grâce à la séparation des données de la vue et du contrôleur
-   Une plus grande souplesse pour organiser le développement du site entre différents développeurs (indépendance des données, de l’affichage (webdesign) et des actions)

## Question 3

Le principe du SOLID utilisé dans cette question est le I. C'est à dire la ségrégation des interfaces. Préférer plusieurs interfaces spécifiques pour chaque client plutôt qu'une seule interface générale

## Question 4

Le Design Patter utilisé dans cette question est l'Adaptateur


## Question 5

Dans cette question le design pattern utilisé est le singleton permettant l'instanciation d'une seule Storage. Il a été implémenté dès la question 2.
Finalement c'est une blague. Une façade à été utilisée (on s'est pris un mur bouuuuuuh)

## Question 6

![](./diagrame_de_classe_final.JPG)