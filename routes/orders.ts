import { Router, Request, Response } from 'express'
//import storage from 'node-persist'

import Controller from '../controller/Controller'
const router = Router()
const controler = new Controller();


router.get('/', async (req: Request, res: Response) => {
  controler.GetOrders(req,res);
})

router.get('/:id', async (req: Request, res: Response) => {
  controler.GetOneOrders(req,res);
})

router.post('/', async (req: Request, res: Response) => {
  controler.AddOrders(req,res);
})

export default router
