import {Contact} from '../types';

export default class Adapter implements Contact{
    public fullname: string;
    public email: string;
    public phone: string;
    public address: string;
    public postalCode: string;
    public city: string;

    constructor(){
        this.fullname = 'abcd'
        this.email = 'abcd'
        this.phone = 'abcd'
        this.address = 'abcd'
        this.postalCode= 'abcd'
        this.city = 'abcd'
    }
}