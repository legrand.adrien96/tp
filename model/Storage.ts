import storage from 'node-persist'
import Order from '../types'
import Adapteur from '../controller/Adapter';
import { orders } from '../data/_orders';

export default class Storage {

    private type: string;
    

     constructor(type:string) {
        this.type = type
        storage.init().then(() => {
            storage.setItem(this.type,orders)
          })
        
    } 
    
    public async getItems():Promise<any>{
        return storage.getItem(this.type);
    }

    public async setItems(s :string):Promise<any>{
        return await storage.setItem(this.type,s);
    }

}
